import 'package:flutter/material.dart';
import 'package:inovationroom/jadwalpiket/jadwalpiket.dart';
import 'package:inovationroom/absensi/absensi.dart';

class MyDrawer extends StatelessWidget {
  final fontstyle = TextStyle(fontSize: 15);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: ExactAssetImage("assets/drawerbackground.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            accountName: Text("Zereth tede lith"),
            accountEmail: Text("1822240050"),
            currentAccountPicture: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => PhotoProfile()));
              },
              child: CircleAvatar(
                backgroundImage: AssetImage("assets/profile.jpg"),
              ),
            ),
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) => JadwalPiket(),
                      ),
                    );
                  },
                  child: ListTile(
                    title: Text(
                      "Jadwal Piket",
                      style: fontstyle,
                    ),
                    leading: Icon(
                      Icons.calendar_today,
                      size: 25,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.of(context).push((MaterialPageRoute(
                      builder: (BuildContext context) => Absensi(),
                    )));
                  },
                  child: ListTile(
                    title: Text(
                      "Absensi",
                      style: fontstyle,
                    ),
                    leading: Icon(
                      Icons.access_time,
                      size: 25,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class PhotoProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[700],
        title: Text("Zereth tede lith"),
      ),
      body: Center(
        child: Image(
          image: AssetImage("assets/profile.jpg"),
        ),
      ),
    );
  }
}
