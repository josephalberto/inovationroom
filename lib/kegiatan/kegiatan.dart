import 'package:flutter/material.dart';
import 'package:inovationroom/mydrawer.dart';

class Kegiatan extends StatefulWidget {
  @override
  _KegiatanState createState() => _KegiatanState();
}

class _KegiatanState extends State<Kegiatan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange[800],
        title: Text("Kegiatan"),
      ),
      drawer: MyDrawer(),
      body: Center(
        child: Text("Kegiatan"),
      ),
    );
  }
}
