import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:inovationroom/kegiatan/kegiatan.dart';
import 'package:inovationroom/scanqrcode/scanqrcode.dart';
import 'package:inovationroom/project/project.dart';
import 'package:flutter/services.dart';
void main() {
  SystemChrome.setEnabledSystemUIOverlays([]);
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int _selectedIndex = 0;
  static List<Widget> _halaman = <Widget>[
    Kegiatan(),
    ScanQRCode(),
    Project()
  ];

  void _gantiHalaman(int index){
    setState((){
      _selectedIndex =  index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _halaman[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text("Beranda"),
        ),
        BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.qrcode),
            title: Text("Qr-Code")
        ),
        BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.tasks),
            title: Text("Project")
        ),
      ],
        selectedItemColor: Colors.deepOrange[800],
        currentIndex: _selectedIndex,
        onTap: _gantiHalaman,
      ),
    );
  }
}
