import 'package:flutter/material.dart';
import 'package:inovationroom/mydrawer.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

//import 'package:random_color/random_color.dart';
import 'package:inovationroom/model/project_model.dart';
import 'package:inovationroom/project/detailproject.dart';

class Project extends StatefulWidget {
  @override
  _ProjectState createState() => _ProjectState();
}

class _ProjectState extends State<Project> {
//  static RandomColor _randomColor = RandomColor();

  List<Project_Model> project = List();

  @override
  void initState() {
    project.add(Project_Model(
        "Project Klinik", "Marhadi Wijaya", "12-8-2019", "19-10-2019", 70.3));
    project.add(Project_Model(
        "Project Inusa", "Edi Sutomo", "19-10-2019", "19-11-2019", 47.0));
    project.add(Project_Model("Project Dislutkan", "Cendy Praksarah",
        "12-2-2018", "12-2-2019", 100.0));
    project.add(Project_Model(
        "Project Inusa", "Edi Sutomo", "19-10-2019", "19-11-2019", 27.0));
    project.add(Project_Model(
        "Project Klinik", "Marhadi Wijaya", "12-8-2019", "19-10-2019", 70.3));
    project.add(Project_Model(
        "Project Inusa", "Edi Sutomo", "19-10-2019", "19-11-2019", 47.0));
    project.add(Project_Model("Project Dislutkan", "Cendy Praksarah",
        "12-2-2018", "12-2-2019", 100.0));
    project.add(Project_Model(
        "Project Inusa", "Edi Sutomo", "19-10-2019", "19-11-2019", 27.0));
    project.add(Project_Model(
        "Project Klinik", "Marhadi Wijaya", "12-8-2019", "19-10-2019", 70.3));
    project.add(Project_Model(
        "Project Inusa", "Edi Sutomo", "19-10-2019", "19-11-2019", 47.0));
    project.add(Project_Model("Project Dislutkan", "Cendy Praksarah",
        "12-2-2018", "12-2-2019", 100.0));
    project.add(Project_Model(
        "Project Inusa", "Edi Sutomo", "19-10-2019", "19-11-2019", 27.0));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepOrange[800],
          title: Text("Project"),
        ),
        drawer: MyDrawer(),
        body: ListView.builder(
          itemCount: project.length,
          itemBuilder: (context, i) {
            return Center(
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 7.5),
                width: screenWidth * 0.90,
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              Detail_Project(project[i].namaproject)),
                    );
                  },
                  child: Card(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                          child: Center(
                            child: Text(
                              project[i].namaproject,
                              style: TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Center(
                          child: Text(
                            "Project Manager : " + project[i].projectmanager,
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ),
                        Center(
                          child: Text(
                            "Start Date: " +
                                project[i].startdate +
                                "\tDeadline: " +
                                project[i].deadline,
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ),
                        LinearPercentIndicator(
                          width: screenWidth * 0.87,
                          animation: true,
                          animationDuration: 1000,
                          lineHeight: 20.0,
//                leading: new Text("left content"),
//                trailing: new Text("right content"),
                          percent: project[i].progress / 100,
                          center: Text(
                            project[i].progress.toString() + "%",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          linearStrokeCap: LinearStrokeCap.butt,
//                      progressColor: _randomColor.randomColor(colorHue: ColorHue.orange,colorSaturation: ColorSaturation.highSaturation),
                          progressColor: Colors.deepOrange[600],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
            ;
          },
        ));
  }
}
