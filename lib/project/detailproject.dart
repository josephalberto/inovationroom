import 'package:flutter/material.dart';

class Detail_Project extends StatelessWidget {
  final String namaproject;
  Detail_Project(this.namaproject);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(namaproject),
        backgroundColor: Colors.deepOrange[800],
      ),
      body: Center(
        child: Text(namaproject),
      ),
    );
  }
}
